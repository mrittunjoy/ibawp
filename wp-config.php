<?php

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
// Include local configuration
if (file_exists(dirname(__FILE__) . '/local-config.php')) {
	include(dirname(__FILE__) . '/local-config.php');
}

// Global DB config
if (!defined('DB_NAME')) {
	define('DB_NAME', 'demo_ibawp');
}
if (!defined('DB_USER')) {
	define('DB_USER', 'ibawp');
}
if (!defined('DB_PASSWORD')) {
	define('DB_PASSWORD', 'bsnl123');
}
if (!defined('DB_HOST')) {
	define('DB_HOST', 'localhost');
}

/** Database Charset to use in creating database tables. */
if (!defined('DB_CHARSET')) {
	define('DB_CHARSET', 'utf8');
}

/** The Database Collate type. Don't change this if in doubt. */
if (!defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '2 Tp)7> LW}UkG&d?J^C_XB XbPhfBpEZZL}Y:#{y]xN,7yrK0$5]8|BA.?Rw;)h');
define('SECURE_AUTH_KEY',  'jR+zz!2g%t r@gzxHwQrH!Y%.sXg@p[U*T T<xvcBzyzj(=+~fi? }pF}~.X!$^U');
define('LOGGED_IN_KEY',    '|Q~fz7L89#`c&eKHEs+C-;w_hAyqn;-Ymud9$d+1DjR&(sIoM.$~#q[rMdc/1+.]');
define('NONCE_KEY',        'Y@2WQ?oGlcyUp vE`+Jh0d>Sb4ycb!GXs0|GH9}ag;=)Da!mw)HW_,TKM{|BY aZ');
define('AUTH_SALT',        '+Phhbf5Yju>I$<4X2PVk8)dECYVXpOBgqNbK+]NVXO{b.Ruv}_L2|n8Xwd}Ca[ep');
define('SECURE_AUTH_SALT', 'FU_1ET`KBDzq+:E_$Dr.K^Na91q3Og59n9Ngnn0&tGRta64Skp|5Q6W{e4.47y=$');
define('LOGGED_IN_SALT',   '%Te^o4|#ry);+ptb-:3-|+m*/< V>lL8/WSkVATuE@#t-IY9YO2C^*VBlQwdd1>!');
define('NONCE_SALT',       '@1+rokp#PM<K,|yA:W`Vuq?(@VB=p|p(|8vYD4xqIFJ-=|6lwN-)/P%RkOnAj:ah');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ibacor';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');



/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if (!defined('WP_DEBUG')) {
	define('WP_DEBUG', false);
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
