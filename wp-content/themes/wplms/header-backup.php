<?php if( is_front_page() ) { ?>
				<div class="container">
				<div class="row">
              <div class="col-md-12">
                <div class="col-md-3 col-sm-3 v_first">
		<style>
        .custom_block img{max-width:80px; padding:30px 0;}
        .custom_block h3{font-weight: 600; text-transform: uppercase; font-size: 17px;margin-bottom:0;}
        //.custom_block h3+p{color:#bbb;margin-top:0;font-size:11px;font-weight:600;text-transform:uppercase;}
        </style>
     <div class="v_module v_text_block  custom_block nothing_selected">
        <p style="text-align: center;"><img src="<?php $upload_dir = wp_upload_dir();echo $upload_dir['baseurl']; ?>/2014/07/icon-man.png" class="circular"/></p>
        <h3 style="text-align: center;">Management Solutions</h3>
        <p style="text-align: center;">IBA specializes in providing the right team to help our clients with Program Management, Acquisition Management, Financial Management, Contract Management, and Risk Management.</p>
           </div>
        </div>
        <div class="col-md-3 col-sm-3">
           <div class="v_module v_text_block  custom_block nothing_selected">
        <p style="text-align: center;"><img src="<?php $upload_dir = wp_upload_dir();echo $upload_dir['baseurl']; ?>/2014/07/icon-agi.png" class="circular"/></p>
        <h3 style="text-align: center;">Agile Solutions</h3>
        <p style="text-align: center;">We are your Agile experts and technology partners. Our Agile services and solutions enable our clients to deliver higher quality products, at an increased rate, while reducing time, cost and risk per release.</p>
           </div>
        </div>
        <div class="col-md-3 col-sm-3">
           <div class="v_module v_text_block  custom_block nothing_selected">
        <p style="text-align: center;"><img src="<?php $upload_dir = wp_upload_dir();echo $upload_dir['baseurl']; ?>/2014/07/icon-tech.png" class="circular"/></p>
        <h3 style="text-align: center;">Technology Solutions</h3>
        <p style="text-align: center;">Through our expertise in the field of Information Technology, IBA simplifies complex processes, expands capabilities and enhances the quality of your products and services.</p>
           </div>
        </div>
        <div class="col-md-3 col-sm-3">
           <div class="v_module v_text_block  custom_block nothing_selected">
        <p style="text-align: center;"><img src="<?php $upload_dir = wp_upload_dir();echo $upload_dir['baseurl']; ?>/2014/07/icon-finan.png" class="circular"/></p>
        <h3 style="text-align: center;">Financial Solutions</h3>
        <p style="text-align: center;">IBA has experience in program financial plan development, preparation and evaluation, budget formulation, budget justification, impact analysis and execution tracking.</p>
           </div>
        </div>
		</div>
		</div>
		<div class="container">
				<div class="row">
              <div class="col-md-12">
               <div class=" content">
		<p style="font-size:36px;">Founded in 1979, IBA is a womanowned professional and technical services firm providing expert program management and advanced technology solutions to government and commercial clients. <a class="form_submit button primary" href="#">Meet Our Team</a></p>
		</div>
        </div>
		</div></div>
	
		
		
       <?php } ?>