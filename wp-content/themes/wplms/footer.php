<section class="stripe fullhomestripe">
<div class="container">

 <div class="row">
<ul class="logo-footer">
<li><div align="center"><img class="imgBox" style="margin-bottom: 15px;" src="<?php $upload_dir = wp_upload_dir();echo $upload_dir['baseurl']; ?>/2014/07/1.png"/></div>
<p class="footer-logo-text">TRICARE Evaluation, Analysis, and Management Support (TEAMS) 
X81XWH08D0032</p></li>
<li><div align="center"><img class="imgBox" style="margin-bottom: 15px;" src="<?php $upload_dir = wp_upload_dir();echo $upload_dir['baseurl']; ?>/2014/07/2.png"/></div>
<p class="footer-logo-text">General Services Administration IT Schedule 70 GS-35F-0100J</p></li>
<li><div align="center"><img class="imgBox" 
 style="margin-bottom: 15px;" src="<?php $upload_dir = wp_upload_dir();echo $upload_dir['baseurl']; ?>/2014/07/3.png"/></div>
<p class="footer-logo-text">General Services Administration Mission Oriented Business Integrated Services (MOBIS)GS-10F-0037K
</p></li>
<li><div align="center"><img class="imgBox" style="margin-bottom: 15px;" src="<?php $upload_dir = wp_upload_dir();echo $upload_dir['baseurl']; ?>/2014/07/4.png"/></div>
<p class="footer-logo-text">General Services Administration Financial and Business Solutions (FABS)GS-23F-0328K
</p></li>
<li><div align="center"><img class="imgBox" style="margin-bottom: 15px;" src="<?php $upload_dir = wp_upload_dir();echo $upload_dir['baseurl']; ?>/2014/07/5.png"/></div>
<p class="footer-logo-text">Telemedicine and Advanced Technology Research Center (TATRC) Research Management Support Services W81XWH-11-A-0024</p></li>
</ul>
</div>
</div>
</section>

<footer>
    <div class="container">
        <div class="row">
            <div class="footertop">
                <?php 
                            if ( !function_exists('dynamic_sidebar')|| !dynamic_sidebar('topfootersidebar') ) : ?>
                <?php endif; ?>
            </div>
        </div>
        <!--<div class="row">
            <div class="footerbottom">
                <?php 
                    if ( !function_exists('dynamic_sidebar')|| !dynamic_sidebar('bottomfootersidebar') ) : ?>
                <?php endif; ?>
            </div>
        </div> -->
    </div> 
    <div id="scrolltop">
        <a><i class="icon-arrow-1-up"></i><span><?php _e('top','vibe'); ?></span></a>
    </div>
</footer>
<div id="footerbottom">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
               <!-- <h2 id="footerlogo"><a href="<?php echo vibe_site_url(); ?>"><img src="<?php  echo apply_filters('wplms_logo_url',VIBE_URL.'/images/logo.png'); ?>" alt="<?php echo get_bloginfo('name'); ?>" /></a></h2>-->
                <?php $copyright=vibe_get_option('copyright'); echo (isset($copyright)?do_shortcode($copyright):'&copy; 2013, All rights reserved.'); ?>
            </div>
            <div class="col-md-7">
                <?php
                    $footerbottom_right = vibe_get_option('footerbottom_right');
                    if(isset($footerbottom_right) && $footerbottom_right){
                        echo '<div id="footer_social_icons">';
                        echo vibe_socialicons();
                        echo '</div>';
                    }else{
                        ?>
                        <div id="footermenu">
                            <?php
                                   /* $args = array(
                                        'theme_location'  => 'footer-menu',
                                        'container'       => '',
                                        'menu_class'      => 'footermenu',
                                        'fallback_cb'     => 'vibe_set_menu',
                                    );
                                    wp_nav_menu( $args ); */   
                            ?>
							
							<div id="footermenu">
                            <ul class="footermenu" id="menu-primary-menu-1"><li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-11 current_page_item menu-item-189"><a href="<?php bloginfo('url'); ?>/about-iba">About IBA</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-190"><a href="<?php bloginfo('url'); ?>/careers">Careers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-191"><a href="<?php bloginfo('url'); ?>/clients">Clients</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-192"><a href="<?php bloginfo('url'); ?>/news">News</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-193"><a href="<?php bloginfo('url'); ?>/services">Services</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-194""><a href="<?php bloginfo('url'); ?>/contact-us"><strong>Contact Us</strong></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-194""><a href="<?php bloginfo('url'); ?>/sitemap"><strong>Sitemap</strong></a></li>
<li class="oval"><a href="http://intranet.ibacorp.us/" target="_blank">login</a></li> 
</ul> 
                     </div>							
							
							
							
                        </div> 
                        <?php
                    }
                ?>
            </div>
        </div>
    </div>
</div>
</div><!-- END PUSHER -->
</div><!-- END MAIN -->
	<!-- SCRIPTS -->
<?php
wp_footer();
?>    
<?php
echo vibe_get_option('google_analytics');
?>
</body>
</html>