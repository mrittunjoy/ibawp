<?php
/**
 * Template Name: ExecutiveLeadership(IBA)
 */
get_header();
//$page_id = get_the_ID();
$page_id=get_the_title($ID);
?>

<section id="content">
	<div class="container">
        <div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="content">     
                
		<?php 
                $image = wp_get_attachment_url( get_post_thumbnail_id($post->ID)); 
                echo "<div class='head-title'>";
				echo "<img src='$image' class='img-responsive' style=' height:auto; width:100%;' /> <hr />";
		echo "</div>";		
                ?>
                     
		   
 <div class="col-md-9 col-sm-8">                
                
                <?php // Display blog posts on any page @ http://m0n.co/l
		$temp = $wp_query; $wp_query= null;
		$wp_query = new WP_Query(); 
		$wp_query->query('category_name=executive&showposts=8'. '&paged='.$paged);//('showposts=6' . '&paged='.$paged);
		while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
        
       
 <div class="col-md-6 for-gap">
                  
          <div class="col-md-12">
             <?php
             if(has_post_thumbnail()) { ?>
                   <div class="img-responsive marg img-adjust">
			       <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'thumbnail') ); ?>
                   <img src="<?php echo $url ?>" class="img-thumbnail"  />
                   </div>
             <?php } else { ?>
             
                   <div class="img-responsive img-thumbnail marg">
			       <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/nonews.jpg" alt="Image Unavailable" class="default-image" />
                   </div>
                   
              <?php } ?>
             </div> 
             
             <div class="col-md-12 h-class"> 
		     <h3><?php the_title(); ?></h3>
             <h5><?php the_sub_title(); ?></h5>
             </div>
             
             
                    <div class="col-md-12">
                       <p>
                       <?php echo substr(get_the_excerpt(), 0,145);  ?>
                       <?php// get_the_excerpt(); ?>
                       <br />
                       <a href="<?php the_permalink(); ?>" title="Read more">Read More</a>
                       </p>
                    </div>
                    
           </div><!-------------col-md-4 mainform------------>  




		       
		<?php endwhile; ?>

		<?php wp_reset_postdata(); pagination(); ?>
                
                
			</div>
                
                         <!----------Sidebar Section--------->

<div class="col-md-3 col-sm-4">
                <div class="sidebar">
                    <?php
                    $sidebar = apply_filters('wplms_sidebar','aboutus-widget',get_the_ID());
                    if ( !function_exists('dynamic_sidebar')|| !dynamic_sidebar($sidebar) ) : ?>
                    <?php endif; ?>
                </div>
            </div>


                        <!----------Sidebar section------------->
                
                </div><!---content------>

		</div>


        </div>
	</div>
</section>
<?php
get_footer();
?>