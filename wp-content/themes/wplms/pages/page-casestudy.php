<?php
/**
 * Template Name: Case Study(IBA)
 */
get_header();
$page_id = get_the_ID();
?>
<section id="title">
	<div class="container">
		<div class="row">
            <div class="col-md-7 col-sm-7">
                <div class="pagetitle">
                    <h1><?php the_title(); ?></h1>
                    <h5><?php the_sub_title(); ?></h5>
                </div>
            </div>
            <div class="col-md-5 col-sm-5">
                <div class="pull-right">
                <?php vibe_breadcrumbs(); ?> 
                </div>
            </div>
        </div>
	</div>
</section>
<section id="content">
	<div class="container">
        <div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="content">     
                
						<?php 
                $image = wp_get_attachment_url( get_post_thumbnail_id($post->ID)); 
                echo "<div class='head-title'>";
				echo "<img src='$image' class='img-responsive' style=' height:auto; width:100%;' /> <hr />";
		echo "</div>";		
                ?>
                      

<div class="col-md-9 col-sm-8"> 
				<?php
				   $temp = $wp_query; $wp_query= null;
		           $wp_query = new WP_Query(); 
		           $wp_query->query('category_name=casestudy&showposts=4'. '&paged='.$paged);//('showposts=6' . '&paged='.$paged);
		           if ( have_posts() ) : while ($wp_query->have_posts()) : $wp_query->the_post();


                    $categories = get_the_category();
                    $cats='<ul>';
                    if($categories){
                        foreach($categories as $category) {
                            $cats .= '<li><a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a></li>';
                        }
                    }
                    $cats .='</ul>';
                        
                       echo ' <div class="blogpost">
                            
                          '.(has_post_thumbnail(get_the_ID())?'
                            <div class="col-md-3 col-sm-3">
							   
                                <a href="#">'.get_the_post_thumbnail(get_the_ID(),'medium').'</a>
															
  </div>':'<div class="featured"><a href="#"><img width="300" height="225"  class="attachment-medium wp-post-image" src=" '.get_stylesheet_directory_uri().'/images/case.png " /></a></div>').'
							
							
							
     <div style="margin-bottom: 20px;" class="col-md-9 col-sm-9'.(has_post_thumbnail(get_the_ID())?'':'').'">
                                <h3>'.get_the_title().'</h3>
                                
                                <p>'.get_the_content().'</p>
                                
                            </div>
                        </div>';

                        
                    endwhile;
                    endif;
                    wp_reset_postdata();
                    pagination();
                ?>
</div>

<!----------Sidebar Section--------->
<div class="col-md-3 col-sm-4">
                <div class="sidebar">
                    <?php
                    $sidebar = apply_filters('wplms_sidebar','clients-widget',get_the_ID());
                    if ( !function_exists('dynamic_sidebar')|| !dynamic_sidebar($sidebar) ) : ?>
                    <?php endif; ?>
                </div>
            </div>
<!----------Sidebar section------------->





			</div>
		</div>
		
        </div>
	</div>
</section>
<?php
get_footer();
?>