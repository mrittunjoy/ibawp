<?php
/****
*
* Template Name: Contact us(IBA)
* 
*
**/

?>


<?php
get_header();

if ( have_posts() ) : while ( have_posts() ) : the_post();

$title=get_post_meta(get_the_ID(),'vibe_title',true);
if(vibe_validate($title)){
?>
<section id="title">
    <div class="container">
    
    
             
    
        <div class="row">
            <div class="col-md-9 col-sm-8">
                <div class="pagetitle">
                    <h1><?php the_title(); ?></h1>
                    <h5><?php the_sub_title(); ?></h5>
                </div>
            </div>
            <div class="col-md-3 col-sm-4">
                <?php
                    $breadcrumbs=get_post_meta(get_the_ID(),'vibe_breadcrumbs',true);
                    if(vibe_validate($breadcrumbs))
                        vibe_breadcrumbs(); 
                ?>
            </div>
        </div>
    </div>
</section>
<?php
}

    $v_add_content = get_post_meta( $post->ID, '_add_content', true );
 
?>
<div class="fluid-container">

<!----map code--->
<div class="fluid-wrapper">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6205.399101017606!2d-77.38815261397525!3d38.95369171451373!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89b647faa545144f%3A0xc92df217d0cbb6b6!2s205+Van+Buren+St+%23150%2C+Herndon%2C+VA+20170%2C+USA!5e0!3m2!1sen!2sin!4v1406381971713" width="100%" height="450" frameborder="0" style="border:0"></iframe>
</div>
<!-----/map code----->

</div>

<section id="content">
    <div class="container">
	<div class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="<?php echo $v_add_content;?>">
                    <?php
                        the_content();
                     ?>
                </div>
            </div>            
            <div class="col-md-6">

                <div class="<?php echo $v_add_content;?>">
                    
                    <!--------Insert below the Contact us Form-------->
					<h3 class="heading"><i class="fa fa-envelope"></i>&nbsp;SEND US A MESSAGE</span></h3>
                   <?php if( function_exists( 'ninja_forms_display_form' ) ){ ninja_forms_display_form( 2 ); } ?>
                    
                    <!--------/ Contact us Form Code block ends-------->
                    
                </div>
            </div>
			</div>
            
            
            
        </div>
    </div>
</section>
<?php
endwhile;
endif;
?>
</div>

<?php
get_footer();
?>