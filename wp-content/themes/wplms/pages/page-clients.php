<?php
/**
 * Template Name: Clients(IBA)
**/

get_header();
if ( have_posts() ) : while ( have_posts() ) : the_post();

$title=get_post_meta(get_the_ID(),'vibe_title',true);
if(vibe_validate($title)){
?>
<section id="title">
    <div class="container">
         <div class="row">
            <div class="col-md-7 col-sm-7">
                <div class="pagetitle">
                    <h1><?php the_title(); ?></h1>
                    <h5><?php the_sub_title(); ?></h5>
                </div>
            </div>
            <div class="col-md-5 col-sm-5">
                <div class="pull-right">
                <?php vibe_breadcrumbs(); ?> 
                </div>
            </div>
        </div>
    </div>
</section>
<?php
}
?>
<section id="content">
    <div class="container">

        <div class="row">
            <div class="col-md-9 col-sm-8">
                <div class="content">
                    <?php
                        the_content();
                     ?>
                </div>
                <?php
                
                endwhile;
                endif;
                ?>
            </div>
            <div class="col-md-3 col-sm-4">
                <div class="sidebar">
                    <?php
                    $sidebar = apply_filters('wplms_sidebar','clients-widget',get_the_ID());
                    if ( !function_exists('dynamic_sidebar')|| !dynamic_sidebar($sidebar) ) : ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
</div>

<?php
get_footer();
?>