<?php

// Essentials
include_once 'includes/config.php';
include_once 'includes/init.php';

// Register & Functions
include_once 'includes/register.php';
include_once 'includes/func.php';


include_once 'includes/ratings.php';

//register Sidebars------------

if(function_exists('register_sidebar'))
{ 
 register_sidebar( array(
        'name' => 'News(IBA) Page Sidebar',
        'id' => 'news-widget',
        'before_widget' => '<div class="widget">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widget_title">',
        'after_title' => '</h4>',
        'description'   => __('This is the News Sidebar widget area by  @stha','vibe')
    ) );



 register_sidebar( array(
        'name' => 'About(IBA) Page Sidebar',
        'id' => 'aboutus-widget',
        'before_widget' => '<div class="widget">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widget_title">',
        'after_title' => '</h4>',
        'description'   => __('This is the About Sidebar widget area by  @stha','vibe')
    ) );
	
 register_sidebar( array(
        'name' => 'Services(IBA) Page Sidebar',
        'id' => 'services-widget',
        'before_widget' => '<div class="widget">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widget_title">',
        'after_title' => '</h4>',
        'description'   => __('This is the Services Sidebar widget area by  @stha','vibe')
    ) );
	
	register_sidebar( array(
        'name' => 'Clients(IBA) Page Sidebar',
        'id' => 'clients-widget',
        'before_widget' => '<div class="widget">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widget_title">',
        'after_title' => '</h4>',
        'description'   => __('This is the Clients Sidebar widget area by  @stha','vibe')
    ) );

register_sidebar( array(
        'name' => 'Contract Vehicles(IBA)Service Page Sidebar',
        'id' => 'contract-widget',
        'before_widget' => '<div class="widget">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widget_title">',
        'after_title' => '</h4>',
        'description'   => __('This is the Contract Vehicles Sidebar widget area by @stha','vibe')
    ) );
	
	register_sidebar( array(
        'name' => 'Careers(IBA) Page Sidebar',
        'id' => 'careers-widget',
        'before_widget' => '<div class="widget">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widget_title">',
        'after_title' => '</h4>',
        'description'   => __('This is the Careers Sidebar widget area by  @stha','vibe')
    ) );

$topspan=vibe_get_option('top_footer_columns');
if(!isset($topspan)) {$topspan = 'col-md-3 col-sm-6';}
     register_sidebar( array(
        'name' => 'Top Footer Sidebar',
        'id' => 'topfootersidebar',
        'before_widget' => '<div class="'.$topspan.'"><div class="footerwidget">',
        'after_widget' => '</div></div>',
        'before_title' => '<h4 class="footertitle">',
        'after_title' => '</h4>',
        'description'   => __('Top Footer widget area / sidebar','vibe')
    ) );


$bottomspan=vibe_get_option('bottom_footer_columns');
if(!isset($bottomspan)) {$bottomspan = 'col-md-4 col-md-4';}
     register_sidebar( array(
        'name' => 'Bottom Footer Sidebar',
        'id' => 'bottomfootersidebar',
        'before_widget' => '<div class="'.$bottomspan.'"><div class="footerwidget">',
        'after_widget' => '</div></div>',
        'before_title' => '<h4 class="footertitle">',
        'after_title' => '</h4>',
        'description'   => __('Bottom Footer widget area / sidebar','vibe')
    ) );

     $sidebars=vibe_get_option('sidebars');
    if(isset($sidebars) && is_array($sidebars)){ 
        foreach($sidebars as $sidebar){ 
            register_sidebar( array(
    		'name' => $sidebar,
    		'id' => $sidebar,
    		'before_widget' => '<div class="widget"><div class="inside">',
    		'after_widget' => '</div></div>',
    		'before_title' => '<h4 class="widgettitle">',
    		'after_title' => '</h4>',
            'description'   => __('Custom sidebar, created from Sidebar Manager','vibe')
    	) );
      }
    }
}  


//end of sidebar--------------






// Customizer
include_once 'includes/customizer/customizer.php';
include_once 'includes/customizer/css.php';


include_once 'includes/vibe-menu.php';

include_once 'includes/author.php';

if ( function_exists('bp_get_signup_allowed')) {
    include_once 'includes/bp-custom.php';
}

include_once '_inc/ajax.php';

//Widgets
//include_once('includes/widgets/custom_widgets.php');
//if ( function_exists('bp_get_signup_allowed')) {
 //include_once('includes/widgets/custom_bp_widgets.php');
//}
//include_once('includes/widgets/advanced_woocommerce_widgets.php');
include_once('includes/widgets/twitter.php');
include_once('includes/widgets/flickr.php');

//Misc
include_once 'includes/sharing.php';
//include_once 'includes/tour.php';

// Options Panel
get_template_part('vibe','options');

?>
<?php
function the_breadcrumb() {
    global $post;
    echo '<ul id="breadcrumbs">';
    if (!is_home()) {
        echo '<li><a href="';
        echo get_option('home');
        echo '">';
        echo bloginfo('name');
        echo '</a></li><li class="separator"> &raquo; </li>';
        if (is_category() || is_single()) {
            echo '<li>';
            the_category(' </li><li class="separator"> &raquo; </li><li> ');
            if (is_single()) {
                echo '</li><li class="separator"> &raquo; </li><li>';
                the_title();
                echo '</li>';
            }
        } elseif (is_page()) {
            if($post->post_parent){
                $anc = get_post_ancestors( $post->ID );
                $title = get_the_title();
                foreach ( $anc as $ancestor ) {
                    $output = '<li><a href="'.

permalink_anchor($ancestor).'" title="'.get_the_title($ancestor).'">'.get_the_title($ancestor).'</a></li> <li class="separator">&raquo;</li>';
                }
                echo $output;
                echo ' '.$title.' ';
            } else {
                echo '<li>'.get_the_title().'</li>';
            }
        }
    }
    elseif (is_tag()) {single_tag_title();}
    elseif (is_day()) {echo"<li>Archive for "; the_time('F jS, Y'); echo'</li>';}
    elseif (is_month()) {echo"<li>Archive for "; the_time('F, Y'); echo'</li>';}
    elseif (is_year()) {echo"<li>Archive for "; the_time('Y'); echo'</li>';}
    elseif (is_author()) {echo"<li>Author Archive"; echo'</li>';}
    elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>Blog Archives"; echo'</li>';}
    elseif (is_search()) {echo"<li>Search Results"; echo'</li>';}
    echo '</ul>';
}
?>








<?php 
// Creating the widget 
class wpb_widget extends WP_Widget {

function __construct() {
parent::__construct(
// Base ID of your widget
'wpb_widget', 

// Widget name will appear in UI
__('Astha Widget', 'wpb_widget_domain'), 

// Widget description
array( 'description' => __( 'This is a simple Asthait widget', 'wpb_widget_domain' ), ) 
);
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
$title = apply_filters( 'widget_title', $instance['title'] );
$cat = apply_filters( 'widget_title', $instance['cat'] );
// before and after widget arguments are defined by themes
echo $args['before_widget'];
if ( ! empty( $title ) )
echo $args['before_title'] .'<i class="fa fa-file-text"></i>  &nbsp;' .$title . $args['after_title'];
//echo $cat;

  echo " <article>    ";
		// Display blog posts on any page @ http://m0n.co/l
		$temp = $wp_query; $wp_query= null;
		$wp_query = new WP_Query(); 
		$wp_query->query("category_name='$cat'&showposts=3");//('showposts=6' . '&paged='.$paged);
		while ($wp_query->have_posts()) : $wp_query->the_post();
        $url = get_post_permalink();
		$name =get_the_title($post->ID);
        echo " <div class='col-md-12 sidebar-class'> ";
		echo "<h4><a href=' $url ' title='Read more'>  " .'<i class="fa fa-flash"></i> &nbsp;' . $name . " </a></h4>";
            		        
         echo " </div>	";       
		 endwhile;
		wp_reset_postdata(); 
   echo "     </article> ";


// This is where you run the code and display the output
//echo __( 'Hello, World!', 'wpb_widget_domain' );
echo $args['after_widget'];
}
		
// Widget Backend 
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
$cat = $instance[ 'cat' ];
}
else {
$title = __( 'New title', 'wpb_widget_domain' );
$cat = __( 'New title', 'wpb_widget_domain' );
}
// Widget admin form
?>

<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>

<p>
<label for="<?php echo $this->get_field_id( 'cat' ); ?>"><?php _e( 'cat:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'cat' ); ?>" name="<?php echo $this->get_field_name( 'cat' ); ?>" type="text" value="<?php echo esc_attr( $cat ); ?>" />
</p>


<?php 
}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
$instance['cat'] = ( ! empty( $new_instance['cat'] ) ) ? strip_tags( $new_instance['cat'] ) : '';
return $instance;
}
} // Class wpb_widget ends here

// Register and load the widget
function wpb_load_widget() {
	register_widget( 'wpb_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );
?> 